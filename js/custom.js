/*------Burger at Home-----*/

document.querySelector('.burger').addEventListener('click', function(){

	document.querySelector('nav').classList.toggle('open');

})

/*----Carousel at Content------*/

$('.myCarousel').slick({
	autoplay: true,
	autoplaySpeed: 3000,
	dots: true,
	prevArrow: '<i class="fas fa-arrow-circle-left"></i>',
	nextArrow: '<i class="fas fa-arrow-circle-right"></i>'
});

/*-----Today set at Contact-----*/

var now = new Date();

var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);

var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

$('#setDate').click(function() {
  $('#theDate').val(today);
});

/*----------Game board-----------*/

var tile = '<div class="tile"></div>';

var treble = 'https://i.pinimg.com/originals/53/cb/ac/53cbac82ad98843c7687a657e5160cef.png'

var treblePos = [0,0] // Top Left

// treblePos[0] // Top
// treblePos[1] // Left

// $('.game-board').html(tile);

for ( var i = 0; i < 25; i++ ) {
  $('.game-board').append('<div class="tile" style="background-color:white"></div>');
}

$('.game-board').append(' <div class="treble"><img src="'+treble+'"></div> ');



$(document).keydown(
  function(event) {
    // console.log(event.which); 
    
    if (event.which == 37) {
     if(treblePos[1] > 0) {
      console.log('LEFT');
      $('.treble').css('left', treblePos[1] - 100);
      treblePos[1] = treblePos[1] - 100;
      }
    }
     if (event.which == 38) {
      if(treblePos[0] > 0) {
      console.log('UP');
      $('.treble').css('top', treblePos[0] - 100);
      treblePos[0] = treblePos[0] - 100;
      }
    }
     if (event.which == 39) {
      if(treblePos[1] < $(window).width()-150) {
      console.log('RIGHT');
      $('.treble').css('left', treblePos[1] + 100);
      treblePos[1] = treblePos[1] + 100;
      }
    }
     if (event.which == 40) {
      if(treblePos[0] < 100) {
      console.log('DOWN');
      $('.treble').css('top', treblePos[0] + 100);
      treblePos[0] = treblePos[0] + 100;
      }
    }
  }
); 

/*--------------LightBox----------*/

$('.gallery a').simpleLightbox();